import { StyleProvider } from "native-base";
import React, { Component } from "react";

import getTheme from "./components";
import variables from "./variables/material";

export default class StyleWrapper extends Component {
  render() {
    return (
        <StyleProvider style={getTheme(variables)}>
            {this.props.children}
        </StyleProvider>
    );
  }
}
