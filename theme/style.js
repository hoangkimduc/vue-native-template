export default {
  content_center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  primary: "#00C9FF"
};
