import axios from "axios"
import config from "../../config"
const VuePlugin = {
  install(Vue, options) {
    axios.defaults.baseURL = `${config.host}${config.api}`;
    // Add a request interceptor
    axios.interceptors.silent = false;
    axios.interceptors.request.use(
      config => {
        console.log("Gui request");
        return config;
      },
      error => {
        console.log(error);
        return Promise.reject(error);
      }
    );

    // Add a response interceptor
    axios.interceptors.response.use(
      response => {
        return response;
      },
      error => {
        console.log("Lỗi request", error);
        if (error.response) {
        } else if (error.message && error.message === "Network Error") {
          console.error(error);
        }
        return Promise.reject(error);
      }
    );

    Vue.prototype.$axios = axios;
  }
};

export default VuePlugin;
