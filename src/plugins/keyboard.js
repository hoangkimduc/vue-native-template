import { Keyboard } from "react-native";

const VuePlugin = {
  install(Vue, options) {
    Keyboard.addListener("keyboardDidShow", () => {
      options.store.dispatch("showFooter", false);
    });

    Keyboard.addListener("keyboardDidHide", () => {
      options.store.dispatch("showFooter", true);
    });
  }
};

export default VuePlugin;
