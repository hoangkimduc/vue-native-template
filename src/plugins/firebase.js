import firebase from 'firebase';
require("firebase/firestore");

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyDjfqgK0yBvg6urtXaHTOxG-EuUAz7fj8Q",
  authDomain: "smartstore-220318.firebaseapp.com",
  databaseURL: "https://smartstore-220318.firebaseio.com",
  projectId: "smartstore-220318",
  storageBucket: "smartstore-220318.appspot.com",
  messagingSenderId: "1087709715208"
};
firebase.initializeApp(firebaseConfig);

// Initialize Cloud Firestore through Firebase
firebase.db = firebase.firestore();

// Disable deprecated features
firebase.db.settings({
  timestampsInSnapshots: true
});
export default firebase;