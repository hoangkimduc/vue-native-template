import { NetInfo } from "react-native";

const VuePlugin = {
  install(Vue, options) {
    NetInfo.addEventListener("connectionChange", connectionInfo => {
      if (connectionInfo.type == "none") {
        options.store.commit("IS_OFFLINE", true);
        Vue.prototype.$statusbar.setHidden(true);
      } else {
        options.store.commit("IS_OFFLINE", false);
        Vue.prototype.$statusbar.setHidden(false);
      }
    });
  }
};

export default VuePlugin;
