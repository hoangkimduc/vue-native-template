import { Permissions, Notifications } from "expo";
import config from "../../config";

const PUSH_ENDPOINT = config.host + "/token";
const VuePlugin = {
  async install(Vue, options) {
    /*
     * Permissions.NOTIFICATIONS
     */
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== "granted") {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== "granted") {
      return;
    }

    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
    console.log("Push Notification token:", token);
    // POST the token to your backend server from where you can retrieve it to send push notifications.
    
    /*
     * Permissions.CAMERA
     */
    await Permissions.askAsync(Permissions.CAMERA)
      .then(status => {
        if (status.status == "granted") {
          console.log("Được phép quyền Camera");
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
};
export default VuePlugin;
