import React from "react";
import { Dimensions } from "react-native";
import {
  createSwitchNavigator,
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";
// Switch navigator component
import MiddlewareScreen from "./Middleware.vue";
import SignInScreen from "./screens/SignIn.vue";
// Tab navigator component
import HomeScreen from "./screens/Home.vue";
import StoresScreen from "./screens/Stores.vue";
import ScanScreen from "./screens/Scan.vue";
import HistoryScreen from "./screens/History.vue";
import HistoryChild from "./screens/HistoryChild.vue";
import ProfileScreen from "./screens/Profile.vue";
// Components
import FooterBar from "./components/FooterBar.vue";

export default createSwitchNavigator(
  {
    Middleware: {
      screen: MiddlewareScreen,
      navigationOptions: {
        header: null
      }
    },
    App: createBottomTabNavigator(
      {
        Home: { screen: HomeScreen },
        Stores: { screen: StoresScreen },
        History: createStackNavigator({
          HistoryMain: {
            screen: HistoryScreen,
            navigationOptions: {
              header: null
            }
          },
          HistoryChild: {
            screen: HistoryChild,
            navigationOptions: {
              header: null
            }
          }
        }),
        Scan: { screen: ScanScreen },
        Profile: { screen: ProfileScreen }
      },
      {
        initialRouteName: "Home",
        tabBarPosition: "bottom",
        swipeEnabled: false,
        animationEnabled: true,
        lazy: true,
        backBehavior: "initialRouteName",
        initialLayout: {
          width: Dimensions.get("window").width,
          height: Dimensions.get("window").height
        },
        tabBarComponent: props => {
          return <FooterBar {...props} />;
        }
      }
    ),
    Auth: createStackNavigator({
      SignIn: {
        screen: SignInScreen,
        navigationOptions: {
          header: null
        }
      }
    })
  },
  {
    initialRouteName: "Middleware"
  }
);
