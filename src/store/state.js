import { Dimensions, Platform, StatusBar, PixelRatio } from "react-native";

export default {
    loading: false,
    is_offline:false,
    is_sign_in: true,
    is_show_footer: true,
    os: Platform.OS,
    device_width: Dimensions.get("window").width,
    device_height: Dimensions.get("window").height,
    status_bar_height: StatusBar.currentHeight,
    pixel_size: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    main_router: null,
    user: null,
    token: null
}