import Vue from "vue-native-core";

export default {
  IS_SHOW_FOOTER(state, val) {
    state.is_show_footer = val;
  },
  IS_OFFLINE(state, val) {
    state.is_offline = val;
  },
  MAIN_ROUTER(state, val) {
    state.main_router = val;
  },
  LOADING(state, val) {
    state.loading = val;
  },
  SOCIAL_LOGIN(state, val) {
    state.social_login = val;
  },
  USER(state, val) {
    Vue.set(state, "user", val);
  },
  TOKEN(state, val) {
    state.token = val;
  }
};
