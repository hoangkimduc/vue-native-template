import {AsyncStorage } from 'react-native';
export default {
  showFooter({ commit }, val) {
    commit("IS_SHOW_FOOTER", val);
  },
  loading({ commit }, val) {
    commit("LOADING", val);
  },
  async setUser({ commit }, val) {
    commit("USER", val);
    if(val)
      await AsyncStorage.setItem("userData", JSON.stringify(val));
  }
};
